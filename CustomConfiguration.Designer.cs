﻿namespace MSN_Status_Multitool
{
    partial class CustomConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iconLabel = new System.Windows.Forms.Label();
            this.iconSelector = new System.Windows.Forms.ComboBox();
            this.text1Label = new System.Windows.Forms.Label();
            this.text1Area = new System.Windows.Forms.TextBox();
            this.text2Area = new System.Windows.Forms.TextBox();
            this.text2Label = new System.Windows.Forms.Label();
            this.text4Area = new System.Windows.Forms.TextBox();
            this.text4Label = new System.Windows.Forms.Label();
            this.text3Area = new System.Windows.Forms.TextBox();
            this.text3Label = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.applyButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // iconLabel
            // 
            this.iconLabel.AutoSize = true;
            this.iconLabel.Location = new System.Drawing.Point(12, 15);
            this.iconLabel.Name = "iconLabel";
            this.iconLabel.Size = new System.Drawing.Size(58, 13);
            this.iconLabel.TabIndex = 0;
            this.iconLabel.Text = "Icon Type:";
            // 
            // iconSelector
            // 
            this.iconSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.iconSelector.FormattingEnabled = true;
            this.iconSelector.Items.AddRange(new object[] {
            "Music",
            "Games",
            "Office"});
            this.iconSelector.Location = new System.Drawing.Point(76, 12);
            this.iconSelector.Name = "iconSelector";
            this.iconSelector.Size = new System.Drawing.Size(193, 21);
            this.iconSelector.TabIndex = 1;
            // 
            // text1Label
            // 
            this.text1Label.AutoSize = true;
            this.text1Label.Location = new System.Drawing.Point(12, 42);
            this.text1Label.Name = "text1Label";
            this.text1Label.Size = new System.Drawing.Size(40, 13);
            this.text1Label.TabIndex = 2;
            this.text1Label.Text = "Text 1:";
            // 
            // text1Area
            // 
            this.text1Area.Location = new System.Drawing.Point(58, 39);
            this.text1Area.Name = "text1Area";
            this.text1Area.Size = new System.Drawing.Size(211, 20);
            this.text1Area.TabIndex = 3;
            // 
            // text2Area
            // 
            this.text2Area.Location = new System.Drawing.Point(58, 65);
            this.text2Area.Name = "text2Area";
            this.text2Area.Size = new System.Drawing.Size(211, 20);
            this.text2Area.TabIndex = 5;
            // 
            // text2Label
            // 
            this.text2Label.AutoSize = true;
            this.text2Label.Location = new System.Drawing.Point(12, 68);
            this.text2Label.Name = "text2Label";
            this.text2Label.Size = new System.Drawing.Size(40, 13);
            this.text2Label.TabIndex = 4;
            this.text2Label.Text = "Text 2:";
            // 
            // text4Area
            // 
            this.text4Area.Location = new System.Drawing.Point(58, 117);
            this.text4Area.Name = "text4Area";
            this.text4Area.Size = new System.Drawing.Size(211, 20);
            this.text4Area.TabIndex = 7;
            // 
            // text4Label
            // 
            this.text4Label.AutoSize = true;
            this.text4Label.Location = new System.Drawing.Point(12, 120);
            this.text4Label.Name = "text4Label";
            this.text4Label.Size = new System.Drawing.Size(40, 13);
            this.text4Label.TabIndex = 6;
            this.text4Label.Text = "Text 4:";
            // 
            // text3Area
            // 
            this.text3Area.Location = new System.Drawing.Point(58, 91);
            this.text3Area.Name = "text3Area";
            this.text3Area.Size = new System.Drawing.Size(211, 20);
            this.text3Area.TabIndex = 9;
            // 
            // text3Label
            // 
            this.text3Label.AutoSize = true;
            this.text3Label.Location = new System.Drawing.Point(12, 94);
            this.text3Label.Name = "text3Label";
            this.text3Label.Size = new System.Drawing.Size(40, 13);
            this.text3Label.TabIndex = 8;
            this.text3Label.Text = "Text 3:";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(12, 143);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // applyButton
            // 
            this.applyButton.Location = new System.Drawing.Point(194, 143);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 11;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // CustomConfiguration
            // 
            this.AcceptButton = this.applyButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(281, 179);
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.text3Area);
            this.Controls.Add(this.text3Label);
            this.Controls.Add(this.text4Area);
            this.Controls.Add(this.text4Label);
            this.Controls.Add(this.text2Area);
            this.Controls.Add(this.text2Label);
            this.Controls.Add(this.text1Area);
            this.Controls.Add(this.text1Label);
            this.Controls.Add(this.iconSelector);
            this.Controls.Add(this.iconLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomConfiguration";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configure \"Custom\"";
            this.Load += new System.EventHandler(this.CustomConfiguration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label iconLabel;
        private System.Windows.Forms.ComboBox iconSelector;
        private System.Windows.Forms.Label text1Label;
        private System.Windows.Forms.TextBox text1Area;
        private System.Windows.Forms.TextBox text2Area;
        private System.Windows.Forms.Label text2Label;
        private System.Windows.Forms.TextBox text4Area;
        private System.Windows.Forms.Label text4Label;
        private System.Windows.Forms.TextBox text3Area;
        private System.Windows.Forms.Label text3Label;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button applyButton;
    }
}