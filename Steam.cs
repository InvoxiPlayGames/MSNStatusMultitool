﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace MSN_Status_Multitool
{
    class Steam
    {
        bool enabled = false;
        Timer updateTimer = new Timer();
        public void init()
        {
            updateTimer.Interval = 10000;
            updateTimer.Tick += new System.EventHandler(this.updateGame);
        }

        public void enable()
        {
            enabled = true;
            updateTimer.Start();
        }

        private void updateGame(object sender, EventArgs e)
        {
            if (enabled)
            {
                HttpWebRequest userrequest = (HttpWebRequest)WebRequest.Create("https://invoxiplaygames.uk/msnstatus/steam.php?game=" + Properties.Settings.Default.steamID);
                userrequest.UserAgent = "MSN Status Multitool Steam/1.0";
                var userresponse = (HttpWebResponse)userrequest.GetResponse();
                using (var responsereader = new StreamReader(userresponse.GetResponseStream()))
                {
                    string response = responsereader.ReadToEnd();
                    var obj = (JObject)JsonConvert.DeserializeObject(response);
                    if ((bool)obj["playing"])
                    {
                        MSNStatus.SetGame((string)obj["game"]);
                    }
                    else
                    {
                        MSNStatus.Clear();
                    }
                }
            }
        }

        public void disable()
        {
            enabled = false;
            updateTimer.Stop();
            MSNStatus.Clear();
        }
    }
}
