﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MSN_Status_Multitool
{
    public partial class SteamConfiguration : Form
    {
        public SteamConfiguration()
        {
            InitializeComponent();
        }

        private void idURLLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SteamIDResolver resolver = new SteamIDResolver();
            resolver.ShowDialog(this);
            idArea.Text = Properties.Settings.Default.steamID;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.steamID = idArea.Text;
            Properties.Settings.Default.Save();
            Close();
        }
    }
}
