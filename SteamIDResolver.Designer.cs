﻿namespace MSN_Status_Multitool
{
    partial class SteamIDResolver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.curlLabel = new System.Windows.Forms.Label();
            this.communityURL = new System.Windows.Forms.TextBox();
            this.getIdButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // curlLabel
            // 
            this.curlLabel.AutoSize = true;
            this.curlLabel.Location = new System.Drawing.Point(12, 15);
            this.curlLabel.Name = "curlLabel";
            this.curlLabel.Size = new System.Drawing.Size(119, 13);
            this.curlLabel.TabIndex = 0;
            this.curlLabel.Text = "Steam Community URL:";
            // 
            // communityURL
            // 
            this.communityURL.Location = new System.Drawing.Point(137, 12);
            this.communityURL.Name = "communityURL";
            this.communityURL.Size = new System.Drawing.Size(346, 20);
            this.communityURL.TabIndex = 1;
            // 
            // getIdButton
            // 
            this.getIdButton.Location = new System.Drawing.Point(431, 38);
            this.getIdButton.Name = "getIdButton";
            this.getIdButton.Size = new System.Drawing.Size(52, 23);
            this.getIdButton.TabIndex = 2;
            this.getIdButton.Text = "Get ID";
            this.getIdButton.UseVisualStyleBackColor = true;
            this.getIdButton.Click += new System.EventHandler(this.getIdButton_Click);
            // 
            // SteamIDResolver
            // 
            this.AcceptButton = this.getIdButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 70);
            this.Controls.Add(this.getIdButton);
            this.Controls.Add(this.communityURL);
            this.Controls.Add(this.curlLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SteamIDResolver";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Get Steam ID";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label curlLabel;
        private System.Windows.Forms.TextBox communityURL;
        private System.Windows.Forms.Button getIdButton;
    }
}