﻿# MSN Status Multitool

A utility to display music and games on your MSN/Windows Live Messenger status from multiple services

**This utility is in _BETA_, there will be bugs and crashes.**

[Download the latest beta release here](https://invoxiplaygames.uk/msnstatus/#downloads) or compile from this repo.

## Supported Services/Apps

|Name|Requirements|
|-|-|
|iTunes|iTunes 12 or above installed (previous versions already have this functionality)|
|Google Play Music|[Google Play Music Desktop Player](https://www.googleplaymusicdesktopplayer.com/) with the JSON API enabled|
|Spotify|Spotify installed and logged in on any device (you must re-sign in in the configuration panel of the mode every hour)|
|Steam|Steam installed and logged in on any device (you must enter your Steam ID in the configuration panel of the mode)|
|Custom|None (you must set up the values for the status in the configuration panel of the mode)|

### Planned future support

* Custom game detection (by scanning running processes)
* [Wiimmfi](https://wiimmfi.de)
* foobar2000
* Spotify via local API/window title? (faster, less hassle for the end user, only current device)
* Steam via local API (same as above)