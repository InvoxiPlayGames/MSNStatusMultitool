﻿namespace MSN_Status_Multitool
{
    partial class SpotifyConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.accessTokenLabel = new System.Windows.Forms.Label();
            this.accessTokenArea = new System.Windows.Forms.TextBox();
            this.expiryLabel = new System.Windows.Forms.Label();
            this.expiryArea = new System.Windows.Forms.TextBox();
            this.getTokenLink = new System.Windows.Forms.LinkLabel();
            this.applyButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.accountLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // accessTokenLabel
            // 
            this.accessTokenLabel.AutoSize = true;
            this.accessTokenLabel.Location = new System.Drawing.Point(12, 15);
            this.accessTokenLabel.Name = "accessTokenLabel";
            this.accessTokenLabel.Size = new System.Drawing.Size(79, 13);
            this.accessTokenLabel.TabIndex = 0;
            this.accessTokenLabel.Text = "Access Token:";
            // 
            // accessTokenArea
            // 
            this.accessTokenArea.Location = new System.Drawing.Point(97, 12);
            this.accessTokenArea.Name = "accessTokenArea";
            this.accessTokenArea.Size = new System.Drawing.Size(465, 20);
            this.accessTokenArea.TabIndex = 1;
            // 
            // expiryLabel
            // 
            this.expiryLabel.AutoSize = true;
            this.expiryLabel.Location = new System.Drawing.Point(27, 41);
            this.expiryLabel.Name = "expiryLabel";
            this.expiryLabel.Size = new System.Drawing.Size(64, 13);
            this.expiryLabel.TabIndex = 2;
            this.expiryLabel.Text = "Expiry Time:";
            // 
            // expiryArea
            // 
            this.expiryArea.Location = new System.Drawing.Point(97, 38);
            this.expiryArea.Name = "expiryArea";
            this.expiryArea.Size = new System.Drawing.Size(465, 20);
            this.expiryArea.TabIndex = 3;
            // 
            // getTokenLink
            // 
            this.getTokenLink.AutoSize = true;
            this.getTokenLink.Location = new System.Drawing.Point(385, 70);
            this.getTokenLink.Name = "getTokenLink";
            this.getTokenLink.Size = new System.Drawing.Size(96, 13);
            this.getTokenLink.TabIndex = 4;
            this.getTokenLink.TabStop = true;
            this.getTokenLink.Text = "Get Access Token";
            this.getTokenLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.getTokenLink_LinkClicked);
            // 
            // applyButton
            // 
            this.applyButton.Location = new System.Drawing.Point(487, 65);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 5;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(12, 65);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // accountLabel
            // 
            this.accountLabel.AutoSize = true;
            this.accountLabel.Location = new System.Drawing.Point(94, 70);
            this.accountLabel.Name = "accountLabel";
            this.accountLabel.Size = new System.Drawing.Size(83, 13);
            this.accountLabel.TabIndex = 7;
            this.accountLabel.Text = "Account: (none)";
            // 
            // SpotifyConfiguration
            // 
            this.AcceptButton = this.applyButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(574, 97);
            this.Controls.Add(this.accountLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.getTokenLink);
            this.Controls.Add(this.expiryArea);
            this.Controls.Add(this.expiryLabel);
            this.Controls.Add(this.accessTokenArea);
            this.Controls.Add(this.accessTokenLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SpotifyConfiguration";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configure \"Spotify\"";
            this.Load += new System.EventHandler(this.SpotifyConfiguration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label accessTokenLabel;
        private System.Windows.Forms.TextBox accessTokenArea;
        private System.Windows.Forms.Label expiryLabel;
        private System.Windows.Forms.TextBox expiryArea;
        private System.Windows.Forms.LinkLabel getTokenLink;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label accountLabel;
    }
}