﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MSN_Status_Multitool
{
    public partial class SpotifyConfiguration : Form
    {
        public SpotifyConfiguration()
        {
            InitializeComponent();
        }

        private void SpotifyConfiguration_Load(object sender, EventArgs e)
        {
            accessTokenArea.Text = Properties.Settings.Default.spotifyAccess;
            expiryArea.Text = Properties.Settings.Default.spotifyExpires.ToString();
            if (Properties.Settings.Default.spotifyUser != "")
            {
                accountLabel.Text = "Account: " + Properties.Settings.Default.spotifyUser;
            }
        }

        private void getTokenLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://accounts.spotify.com/en/authorize?client_id=5e96ff643c3340b292cdf5629b295e08&response_type=token&redirect_uri=https:%2F%2Finvoxiplaygames.uk%2Fmsnstatus%2Fspotify.php&scope=user-read-currently-playing%20user-read-playback-state");
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.spotifyAccess = accessTokenArea.Text;
            Properties.Settings.Default.spotifyExpires = System.Convert.ToInt32(expiryArea.Text);
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
