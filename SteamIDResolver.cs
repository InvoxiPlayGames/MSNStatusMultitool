﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;

namespace MSN_Status_Multitool
{
    public partial class SteamIDResolver : Form
    {
        public SteamIDResolver()
        {
            InitializeComponent();
        }

        private void getIdButton_Click(object sender, EventArgs e)
        {
            string base64URL = Convert.ToBase64String(Encoding.UTF8.GetBytes(communityURL.Text));
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://invoxiplaygames.uk/msnstatus/steam.php?url=" + base64URL);
            request.UserAgent = "MSN Status Multitool SteamID/1.0";
            var userresponse = (HttpWebResponse)request.GetResponse();
            using (var responsereader = new StreamReader(userresponse.GetResponseStream()))
            {
                string response = responsereader.ReadToEnd();
                var obj = (JObject)JsonConvert.DeserializeObject(response);
                if ((bool)obj["success"])
                {
                    Properties.Settings.Default.steamID = (string)obj["id"];
                    Properties.Settings.Default.Save();
                    Close();
                }
                else
                {
                    MessageBox.Show("Could not get a Steam ID from that URL.", "Steam ID Resolver", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
