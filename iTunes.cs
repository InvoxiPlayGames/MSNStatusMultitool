﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTunesLib;
using System.Windows.Forms;

namespace MSN_Status_Multitool
{
    class iTunes
    {
        bool enabled = false;
        iTunesApp app = new iTunesApp();
        Timer updateTimer = new Timer();

        public void init()
        {
            updateTimer.Interval = 1000;
            updateTimer.Tick += new System.EventHandler(this.updatePlayer);
        }

        public void enable()
        {
            enabled = true;
            updateTimer.Start();
        }

        private void updatePlayer(object sender, EventArgs e)
        {
            if (enabled)
            {
                IITTrack track = app.CurrentTrack;
                if (app.PlayerState == ITPlayerState.ITPlayerStatePlaying)
                {
                    MSNStatus.SetMusic(track.Artist, track.Name, track.Album);
                }
                else if (app.PlayerState == ITPlayerState.ITPlayerStateStopped)
                {
                    if (track == null)
                    {
                        MSNStatus.Clear();
                    }
                    else
                    {
                        MSNStatus.SetMusic(track.Artist, track.Name + " (paused)", track.Album);
                    }
                }
            }
        }

        public void disable()
        {
            enabled = false;
            updateTimer.Stop();
            MSNStatus.Clear();
        }
    }
}
