﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MSN_Status_Multitool
{
    public partial class CustomConfiguration : Form
    {
        public CustomConfiguration()
        {
            InitializeComponent();
        }

        private void CustomConfiguration_Load(object sender, EventArgs e)
        {
            text1Area.Text = Properties.Settings.Default.custom1;
            text2Area.Text = Properties.Settings.Default.custom2;
            text3Area.Text = Properties.Settings.Default.custom3;
            text4Area.Text = Properties.Settings.Default.custom4;
            iconSelector.SelectedItem = Properties.Settings.Default.customIconType;
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.custom1 = text1Area.Text;
            Properties.Settings.Default.custom2 = text2Area.Text;
            Properties.Settings.Default.custom3 = text3Area.Text;
            Properties.Settings.Default.custom4 = text4Area.Text;
            Properties.Settings.Default.customIconType = (string)iconSelector.SelectedItem;
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
