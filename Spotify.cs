﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace MSN_Status_Multitool
{
    class Spotify
    {
        bool enabled = false;
        Timer updateTimer = new Timer();
        public void init()
        {
            updateTimer.Interval = 2000;
            updateTimer.Tick += new System.EventHandler(this.updatePlayer);
        }

        public void enable()
        {
            if (!verify())
            {
                MessageBox.Show("Spotify not authorised - please (re-)configure!");
            }
            else
            {
                enabled = true;
                updateTimer.Start();
            }
        }

        public void update()
        {
            if (enabled)
            {
                if (!verify())
                {
                    updateTimer.Stop();
                    MessageBox.Show("Spotify not authorised - please (re-)configure!");
                }
                else
                {
                    updateTimer.Start();
                }
            }
        }

        private bool verify()
        {
            int unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            if (unixTimestamp > Properties.Settings.Default.spotifyExpires)
            {
                return false;
            }
            else
            {
                HttpWebRequest userrequest = (HttpWebRequest)WebRequest.Create("https://api.spotify.com/v1/me");
                userrequest.Headers.Add("Authorization: Bearer " + Properties.Settings.Default.spotifyAccess);
                userrequest.UserAgent = "MSN Status Multitool Spotify/1.0";
                var userresponse = (HttpWebResponse)userrequest.GetResponse();
                if (userresponse.StatusCode != HttpStatusCode.OK)
                {
                    return false;
                }
                else
                {
                    using (var responsereader = new StreamReader(userresponse.GetResponseStream()))
                    {
                        string response = responsereader.ReadToEnd();
                        var obj = (JObject)JsonConvert.DeserializeObject(response);
                        Properties.Settings.Default.spotifyUser = (string)obj["id"];
                        Properties.Settings.Default.Save();
                        return true;
                    }
                }
            }
        }

        private void updatePlayer(object sender, EventArgs e)
        {
            if (enabled)
            {
                try
                {
                    HttpWebRequest userrequest = (HttpWebRequest)WebRequest.Create("https://api.spotify.com/v1/me/player/currently-playing");
                    userrequest.Headers.Add("Authorization: Bearer " + Properties.Settings.Default.spotifyAccess);
                    userrequest.UserAgent = "MSN Status Multitool Spotify/1.0";
                    var userresponse = (HttpWebResponse)userrequest.GetResponse();
                    if (userresponse.StatusCode != HttpStatusCode.OK)
                    {

                    }
                    else
                    {
                        using (var responsereader = new StreamReader(userresponse.GetResponseStream()))
                        {
                            string response = responsereader.ReadToEnd();
                            var obj = (JObject)JsonConvert.DeserializeObject(response);
                            if ((string)obj["currently_playing_type"] != "ad")
                            {
                                if (obj["item"] != null)
                                {
                                    var songItem = (JObject)obj["item"];
                                    var songName = (string)songItem["name"];
                                    var albumItem = (JObject)songItem["album"];
                                    var albumName = (string)albumItem["name"];
                                    var artistItem = (JObject)songItem["artists"].FirstOrDefault();
                                    var artistName = (string)artistItem["name"];
                                    if ((bool)obj["is_playing"])
                                    {
                                        MSNStatus.SetMusic(artistName, songName, albumName);
                                    }
                                    else
                                    {
                                        MSNStatus.SetMusic(artistName, songName + " (paused)", albumName);
                                    }
                                }
                                else
                                {
                                    MSNStatus.Clear();
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    // Fine to ignore, either rate limited or can't catch up. Ah well.
                }
            }
        }

        public void disable()
        {
            enabled = false;
            updateTimer.Stop();
            MSNStatus.Clear();
        }
    }
}
