﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MSN_Status_Multitool
{
    public partial class Main : Form
    {
        string currentMode = "";
        // iTunes itunesMode = new iTunes();
        iTunes itunesMode;
        Spotify spotifyMode = new Spotify();
        Steam steamMode = new Steam();
        Custom customMode = new Custom();
        GPMDP gpmdpMode = new GPMDP();
        SpotifyConfiguration spotifyConfig = new SpotifyConfiguration();
        SteamConfiguration steamConfig = new SteamConfiguration();
        CustomConfiguration customConfig = new CustomConfiguration();

        public Main()
        {
            InitializeComponent();
        }

        private void modeSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.lastSelected = modeSelector.SelectedIndex;
            Properties.Settings.Default.Save();
            configureLink.Text = "Configure " + modeSelector.SelectedItem;
            switch (modeSelector.SelectedItem)
            {
                case "iTunes (modern)":
                case "Google Play Music":
                case "None":
                    configureLink.Enabled = false;
                    break;
                default:
                    configureLink.Enabled = true;
                    break;
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            if (Registry.CurrentUser.OpenSubKey("SOFTWARE\\Apple Computer, Inc.\\iTunes") != null)
            {
                itunesMode = new iTunes();
                itunesMode.init();
            }
            else
            {
                modeSelector.Items.Remove("iTunes (modern)");
            }
            
            spotifyMode.init();
            steamMode.init();
            gpmdpMode.init();
            
            modeSelector.SelectedIndex = Properties.Settings.Default.lastSelected;
            runOnStartup.Checked = Properties.Settings.Default.runOnStartup;
            minimizeToToolbar.Checked = Properties.Settings.Default.minimizeToToolbar;
            startMinimized.Checked = Properties.Settings.Default.startMinimized;
            enableOnLoad.Checked = Properties.Settings.Default.enableOnLoad;
            
            if (Properties.Settings.Default.enableOnLoad)
            {
                modeConfirm_Click(sender, e);
            }
            if (startMinimized.Checked)
            {
                WindowState = FormWindowState.Minimized;

            }
            
            if (minimizeToToolbar.Checked && startMinimized.Checked)
            {
                BeginInvoke(new MethodInvoker(delegate
                {
                    Hide();
                }));
            }
        }
        private void Main_Close(object sender, EventArgs e)
        {
            this.Hide();
            disableCurrent();
            System.Threading.Thread.Sleep(100);
            MSNStatus.Clear();
            System.Threading.Thread.Sleep(100);
            Properties.Settings.Default.Save();
            System.Threading.Thread.Sleep(100);
            Application.Exit();
        }

        private void configureLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            switch (modeSelector.SelectedItem)
            {
                case "Spotify":
                    spotifyConfig.ShowDialog(this);
                    spotifyMode.update();
                    break;
                case "Steam":
                    steamConfig.ShowDialog(this);
                    break;
                case "Custom":
                    customConfig.ShowDialog(this);
                    customMode.update();
                    break;
            }
        }

        private void modeConfirm_Click(object sender, EventArgs e)
        {
            disableCurrent();
            switch (modeSelector.SelectedItem)
            {
                case "Custom":
                    currentMode = "Custom";
                    customMode.enable();
                    disableButton.Enabled = true;
                    enabledLabel.Text = currentMode;
                    break;
                case "iTunes (modern)":
                    currentMode = "iTunes";
                    itunesMode.enable();
                    disableButton.Enabled = true;
                    enabledLabel.Text = currentMode;
                    break;
                case "Spotify":
                    currentMode = "Spotify";
                    spotifyMode.enable();
                    disableButton.Enabled = true;
                    enabledLabel.Text = currentMode;
                    break;
                case "Steam":
                    currentMode = "Steam";
                    steamMode.enable();
                    disableButton.Enabled = true;
                    enabledLabel.Text = currentMode;
                    break;
                case "Google Play Music":
                    currentMode = "Google Play Music";
                    gpmdpMode.enable();
                    disableButton.Enabled = true;
                    enabledLabel.Text = currentMode;
                    break;
                case "None":
                    disableCurrent();
                    break;
            }
        }

        private void disableCurrent()
        {
            disableButton.Enabled = false;
            switch (currentMode)
            {
                case "Custom":
                    currentMode = "";
                    customMode.disable();
                    break;
                case "iTunes":
                    currentMode = "";
                    itunesMode.disable();
                    break;
                case "Spotify":
                    currentMode = "";
                    spotifyMode.disable();
                    break;
                case "Steam":
                    currentMode = "";
                    steamMode.disable();
                    break;
                case "Google Play Music":
                    currentMode = "";
                    gpmdpMode.disable();
                    break;
            }
            enabledLabel.Text = "(none)";
        }

        private void disableButton_Click(object sender, EventArgs e)
        {
            disableCurrent();
        }

        private void runOnStartup_CheckedChanged(object sender, EventArgs e)
        {
            if (runOnStartup.Checked)
            {
                Properties.Settings.Default.runOnStartup = true;
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
                {
                    key.SetValue("MSN Status Multitool", "\"" + Application.ExecutablePath + "\"");
                }
                Properties.Settings.Default.Save();
            }
            else
            {
                Properties.Settings.Default.runOnStartup = false;
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
                {
                    key.DeleteValue("MSN Status Multitool");
                }
                Properties.Settings.Default.Save();
            }
        }

        private void meLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://invoxiplaygames.uk/msnstatus/");
        }

        private void enableOnLoad_CheckedChanged(object sender, EventArgs e)
        {
            if (enableOnLoad.Checked)
            {
                Properties.Settings.Default.enableOnLoad = true;
            }
            else
            {
                Properties.Settings.Default.enableOnLoad = false;
            }
        }

        private void toolbarIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }
        private void Main_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized && minimizeToToolbar.Checked)
            {
                Hide();
            }
        }

        private void openToolItem_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void quitItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void minimizeToToolbar_CheckedChanged(object sender, EventArgs e)
        {
            if (minimizeToToolbar.Checked)
            {
                Properties.Settings.Default.minimizeToToolbar = true;
            }
            else
            {
                Properties.Settings.Default.minimizeToToolbar = false;
            }
        }

        private void startMinimized_CheckedChanged(object sender, EventArgs e)
        {
            if (startMinimized.Checked)
            {
                Properties.Settings.Default.startMinimized = true;
            }
            else
            {
                Properties.Settings.Default.startMinimized = false;
            }
        }
    }
}