﻿namespace MSN_Status_Multitool
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.createdByLabel = new System.Windows.Forms.Label();
            this.meLabel = new System.Windows.Forms.LinkLabel();
            this.modeLabel = new System.Windows.Forms.Label();
            this.modeSelector = new System.Windows.Forms.ComboBox();
            this.modeConfirm = new System.Windows.Forms.Button();
            this.versionLabel = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.configureLink = new System.Windows.Forms.LinkLabel();
            this.runOnStartup = new System.Windows.Forms.CheckBox();
            this.minimizeToToolbar = new System.Windows.Forms.CheckBox();
            this.enableOnLoad = new System.Windows.Forms.CheckBox();
            this.disableButton = new System.Windows.Forms.Button();
            this.currentlyEnabled = new System.Windows.Forms.Label();
            this.enabledLabel = new System.Windows.Forms.Label();
            this.toolbarIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.toolbarMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openToolItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startMinimized = new System.Windows.Forms.CheckBox();
            this.toolbarMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // createdByLabel
            // 
            this.createdByLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.createdByLabel.AutoSize = true;
            this.createdByLabel.Location = new System.Drawing.Point(12, 168);
            this.createdByLabel.Name = "createdByLabel";
            this.createdByLabel.Size = new System.Drawing.Size(58, 13);
            this.createdByLabel.TabIndex = 0;
            this.createdByLabel.Text = "Created by";
            // 
            // meLabel
            // 
            this.meLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.meLabel.AutoSize = true;
            this.meLabel.Location = new System.Drawing.Point(66, 168);
            this.meLabel.Name = "meLabel";
            this.meLabel.Size = new System.Drawing.Size(88, 13);
            this.meLabel.TabIndex = 1;
            this.meLabel.TabStop = true;
            this.meLabel.Text = "InvoxiPlayGames";
            this.meLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.meLabel_LinkClicked);
            // 
            // modeLabel
            // 
            this.modeLabel.AutoSize = true;
            this.modeLabel.Location = new System.Drawing.Point(12, 16);
            this.modeLabel.Name = "modeLabel";
            this.modeLabel.Size = new System.Drawing.Size(37, 13);
            this.modeLabel.TabIndex = 2;
            this.modeLabel.Text = "Mode:";
            // 
            // modeSelector
            // 
            this.modeSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modeSelector.FormattingEnabled = true;
            this.modeSelector.Items.AddRange(new object[] {
            "iTunes (modern)",
            "Google Play Music",
            "Spotify",
            "Steam",
            "Custom",
            "None"});
            this.modeSelector.Location = new System.Drawing.Point(55, 12);
            this.modeSelector.Name = "modeSelector";
            this.modeSelector.Size = new System.Drawing.Size(162, 21);
            this.modeSelector.TabIndex = 3;
            this.modeSelector.SelectedIndexChanged += new System.EventHandler(this.modeSelector_SelectedIndexChanged);
            // 
            // modeConfirm
            // 
            this.modeConfirm.Location = new System.Drawing.Point(223, 10);
            this.modeConfirm.Name = "modeConfirm";
            this.modeConfirm.Size = new System.Drawing.Size(64, 23);
            this.modeConfirm.TabIndex = 4;
            this.modeConfirm.Text = "Enable";
            this.modeConfirm.UseVisualStyleBackColor = true;
            this.modeConfirm.Click += new System.EventHandler(this.modeConfirm_Click);
            // 
            // versionLabel
            // 
            this.versionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.versionLabel.AutoSize = true;
            this.versionLabel.Location = new System.Drawing.Point(241, 168);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(46, 13);
            this.versionLabel.TabIndex = 5;
            this.versionLabel.Text = "beta 1.0";
            // 
            // titleLabel
            // 
            this.titleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(10, 148);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(178, 20);
            this.titleLabel.TabIndex = 6;
            this.titleLabel.Text = "MSN Status Multitool";
            // 
            // configureLink
            // 
            this.configureLink.AutoSize = true;
            this.configureLink.Location = new System.Drawing.Point(54, 36);
            this.configureLink.Name = "configureLink";
            this.configureLink.Size = new System.Drawing.Size(95, 13);
            this.configureLink.TabIndex = 7;
            this.configureLink.TabStop = true;
            this.configureLink.Text = "Configure {MODE}";
            this.configureLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.configureLink_LinkClicked);
            // 
            // runOnStartup
            // 
            this.runOnStartup.AutoSize = true;
            this.runOnStartup.Location = new System.Drawing.Point(12, 96);
            this.runOnStartup.Name = "runOnStartup";
            this.runOnStartup.Size = new System.Drawing.Size(145, 17);
            this.runOnStartup.TabIndex = 8;
            this.runOnStartup.Text = "Run on Windows Startup";
            this.runOnStartup.UseVisualStyleBackColor = true;
            this.runOnStartup.CheckedChanged += new System.EventHandler(this.runOnStartup_CheckedChanged);
            // 
            // minimizeToToolbar
            // 
            this.minimizeToToolbar.AutoSize = true;
            this.minimizeToToolbar.Location = new System.Drawing.Point(12, 119);
            this.minimizeToToolbar.Name = "minimizeToToolbar";
            this.minimizeToToolbar.Size = new System.Drawing.Size(117, 17);
            this.minimizeToToolbar.TabIndex = 9;
            this.minimizeToToolbar.Text = "Minimize to Toolbar";
            this.minimizeToToolbar.UseVisualStyleBackColor = true;
            this.minimizeToToolbar.CheckedChanged += new System.EventHandler(this.minimizeToToolbar_CheckedChanged);
            // 
            // enableOnLoad
            // 
            this.enableOnLoad.AutoSize = true;
            this.enableOnLoad.Location = new System.Drawing.Point(172, 96);
            this.enableOnLoad.Name = "enableOnLoad";
            this.enableOnLoad.Size = new System.Drawing.Size(101, 17);
            this.enableOnLoad.TabIndex = 11;
            this.enableOnLoad.Text = "Enable on Load";
            this.enableOnLoad.UseVisualStyleBackColor = true;
            this.enableOnLoad.CheckedChanged += new System.EventHandler(this.enableOnLoad_CheckedChanged);
            // 
            // disableButton
            // 
            this.disableButton.Enabled = false;
            this.disableButton.Location = new System.Drawing.Point(223, 59);
            this.disableButton.Name = "disableButton";
            this.disableButton.Size = new System.Drawing.Size(64, 23);
            this.disableButton.TabIndex = 12;
            this.disableButton.Text = "Disable";
            this.disableButton.UseVisualStyleBackColor = true;
            this.disableButton.Click += new System.EventHandler(this.disableButton_Click);
            // 
            // currentlyEnabled
            // 
            this.currentlyEnabled.AutoSize = true;
            this.currentlyEnabled.Location = new System.Drawing.Point(11, 64);
            this.currentlyEnabled.Name = "currentlyEnabled";
            this.currentlyEnabled.Size = new System.Drawing.Size(93, 13);
            this.currentlyEnabled.TabIndex = 13;
            this.currentlyEnabled.Text = "Currently Enabled:";
            // 
            // enabledLabel
            // 
            this.enabledLabel.AutoSize = true;
            this.enabledLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enabledLabel.Location = new System.Drawing.Point(101, 64);
            this.enabledLabel.Name = "enabledLabel";
            this.enabledLabel.Size = new System.Drawing.Size(37, 13);
            this.enabledLabel.TabIndex = 14;
            this.enabledLabel.Text = "(none)";
            // 
            // toolbarIcon
            // 
            this.toolbarIcon.ContextMenuStrip = this.toolbarMenu;
            this.toolbarIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("toolbarIcon.Icon")));
            this.toolbarIcon.Text = "MSN Status Multitool";
            this.toolbarIcon.Visible = true;
            this.toolbarIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.toolbarIcon_MouseDoubleClick);
            // 
            // toolbarMenu
            // 
            this.toolbarMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolItem,
            this.quitItem});
            this.toolbarMenu.Name = "toolbarMenu";
            this.toolbarMenu.Size = new System.Drawing.Size(227, 48);
            // 
            // openToolItem
            // 
            this.openToolItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.openToolItem.Name = "openToolItem";
            this.openToolItem.Size = new System.Drawing.Size(226, 22);
            this.openToolItem.Text = "Open MSN Status Multitool";
            this.openToolItem.Click += new System.EventHandler(this.openToolItem_Click);
            // 
            // quitItem
            // 
            this.quitItem.Name = "quitItem";
            this.quitItem.Size = new System.Drawing.Size(226, 22);
            this.quitItem.Text = "Quit";
            this.quitItem.Click += new System.EventHandler(this.quitItem_Click);
            // 
            // startMinimized
            // 
            this.startMinimized.AutoSize = true;
            this.startMinimized.Location = new System.Drawing.Point(172, 119);
            this.startMinimized.Name = "startMinimized";
            this.startMinimized.Size = new System.Drawing.Size(97, 17);
            this.startMinimized.TabIndex = 15;
            this.startMinimized.Text = "Start Minimized";
            this.startMinimized.UseVisualStyleBackColor = true;
            this.startMinimized.CheckedChanged += new System.EventHandler(this.startMinimized_CheckedChanged);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 190);
            this.Controls.Add(this.startMinimized);
            this.Controls.Add(this.enabledLabel);
            this.Controls.Add(this.currentlyEnabled);
            this.Controls.Add(this.disableButton);
            this.Controls.Add(this.enableOnLoad);
            this.Controls.Add(this.minimizeToToolbar);
            this.Controls.Add(this.runOnStartup);
            this.Controls.Add(this.configureLink);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.modeConfirm);
            this.Controls.Add(this.modeSelector);
            this.Controls.Add(this.modeLabel);
            this.Controls.Add(this.meLabel);
            this.Controls.Add(this.createdByLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "MSN Status Multitool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_Close);
            this.Load += new System.EventHandler(this.Main_Load);
            this.Resize += new System.EventHandler(this.Main_Resize);
            this.toolbarMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label createdByLabel;
        private System.Windows.Forms.LinkLabel meLabel;
        private System.Windows.Forms.Label modeLabel;
        private System.Windows.Forms.ComboBox modeSelector;
        private System.Windows.Forms.Button modeConfirm;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.LinkLabel configureLink;
        private System.Windows.Forms.CheckBox runOnStartup;
        private System.Windows.Forms.CheckBox minimizeToToolbar;
        private System.Windows.Forms.CheckBox enableOnLoad;
        private System.Windows.Forms.Button disableButton;
        private System.Windows.Forms.Label currentlyEnabled;
        private System.Windows.Forms.Label enabledLabel;
        private System.Windows.Forms.NotifyIcon toolbarIcon;
        private System.Windows.Forms.ContextMenuStrip toolbarMenu;
        private System.Windows.Forms.ToolStripMenuItem openToolItem;
        private System.Windows.Forms.ToolStripMenuItem quitItem;
        private System.Windows.Forms.CheckBox startMinimized;
    }
}

