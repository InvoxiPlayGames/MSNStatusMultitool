﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSN_Status_Multitool
{
    class Custom
    {
        public bool enabled = false;

        public void enable()
        {
            enabled = true;
            MSNStatus.Clear();
            MsnIconTypes icon = MsnIconTypes.None;
            switch (Properties.Settings.Default.customIconType)
            {
                case "None":
                    icon = MsnIconTypes.None;
                    break;
                case "Music":
                    icon = MsnIconTypes.Music;
                    break;
                case "Games":
                    icon = MsnIconTypes.Games;
                    break;
                case "Office":
                    icon = MsnIconTypes.Office;
                    break;
            }
            MSNStatus.Set(true, icon, Properties.Settings.Default.custom1, Properties.Settings.Default.custom2, Properties.Settings.Default.custom3, Properties.Settings.Default.custom4);
        }

        public void update()
        {
            if (enabled)
            {
                MsnIconTypes icon = MsnIconTypes.None;
                switch (Properties.Settings.Default.customIconType)
                {
                    case "None":
                        icon = MsnIconTypes.None;
                        break;
                    case "Music":
                        icon = MsnIconTypes.Music;
                        break;
                    case "Games":
                        icon = MsnIconTypes.Games;
                        break;
                    case "Office":
                        icon = MsnIconTypes.Office;
                        break;
                }
                MSNStatus.Set(true, icon, Properties.Settings.Default.custom1, Properties.Settings.Default.custom2, Properties.Settings.Default.custom3, Properties.Settings.Default.custom4);
            }
        }

        public void disable()
        {
            enabled = false;
            MSNStatus.Clear();
        }
    }
}
