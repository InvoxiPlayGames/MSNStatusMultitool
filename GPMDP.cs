﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace MSN_Status_Multitool
{
    class GPMDP
    {
        bool enabled = false;
        Timer updateTimer = new Timer();
        public void init()
        {
            updateTimer.Interval = 2000;
            updateTimer.Tick += new System.EventHandler(this.updatePlayer);
        }

        public void enable()
        {
            enabled = true;
            updateTimer.Start();
        }

        private void updatePlayer(object sender, EventArgs e)
        {
            if (enabled)
            {
                try
                {
                    var obj = (JObject)JsonConvert.DeserializeObject(File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Google Play Music Desktop Player\\json_store\\playback.json"));
                    var songObj = obj["song"];
                    var songTitle = (string)songObj["title"];
                    var songArtist = (string)songObj["artist"];
                    var songAlbum = (string)songObj["album"];
                    var songPlaying = (bool)obj["playing"];
                    if (songPlaying)
                    {
                        MSNStatus.SetMusic(songArtist, songTitle, songAlbum);
                    }
                    else
                    {
                        MSNStatus.SetMusic(songArtist, songTitle + " (paused)", songAlbum);
                    }
                }
                catch (Exception)
                {
                    // Fine to skip this, usually just can't read the file (timing)
                }
            }
        }

        public void disable()
        {
            enabled = false;
            updateTimer.Stop();
            MSNStatus.Clear();
        }
    }
}
